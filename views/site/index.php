<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
<div class="jumbotron">
        <hl>Consultas de Selección 1</h1>
    </div>
    <div class="body-content">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consultas</h3>
                        <p>Poblacion total española</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1'], ['class' => 'btn btn-primary']) ?>
                            
                        </p>
                    </div>
                </div>
            </div>
        </div>

</div>

